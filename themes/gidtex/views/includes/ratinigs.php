<?php if($appModule != "cars" && $appModule != "offers"){ ?>

      <!-- Start Review Total -->


      <?php if(!empty($avgReviews->overall)){ ?>
      <div class="panel-body panel panel-default">
<!--        <h4><strong><strong>--><?php //echo trans('035'); ?><!--</strong> --><?php //echo $avgReviews->overall;?><!--/10</h4>-->
        <hr>
        <div class="clearfix"></div>
        <?php } ?>
        <!-- End Review Total -->
        <?php } ?>
        <!-- Start Hotel Reviews bars -->
        <?php if($appModule == "hotels" && !empty($avgReviews->overall)){ ?>
        <div class="row RTL">
          <div class="col-xs-12">
            <div class="col-xs-2 col-md-4 col-lg-1 go-right">
              <label class="text-left"><?php echo trans('030');?></label>
            </div>
            <div class="col-xs-9 col-md-8 col-lg-11 go-left">
              <div class="progress">
                <div class="progress-bar progress-bar-primary go-right" role="progressbar" aria-valuenow="20"
                  aria-valuemin="0" aria-valuemax="10" style="width: <?php echo $avgReviews->clean * 10;?>%">
                  <span class="sr-only"></span>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-2 col-md-4 col-lg-1 go-right">
              <label class="text-left"><?php echo trans('031');?></label>
            </div>
            <div class="col-xs-9 col-md-8 col-lg-11 go-left">
              <div class="progress">
                <div class="progress-bar progress-bar-primary go-right" role="progressbar" aria-valuenow="20"
                  aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $avgReviews->comfort * 10;?>%">
                  <span class="sr-only"></span>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-2 col-md-4 col-lg-1 go-right">
              <label class="text-left"><?php echo trans('032');?></label>
            </div>
            <div class="col-xs-9 col-md-8 col-lg-11 go-left">
              <div class="progress">
                <div class="progress-bar progress-bar-primary go-right" role="progressbar" aria-valuenow="20"
                  aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $avgReviews->location * 10;?>%">
                  <span class="sr-only"></span>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-2 col-md-4 col-lg-1 go-right">
              <label class="text-left"><?php echo trans('033');?></label>
            </div>
            <div class="col-xs-9 col-md-8 col-lg-11 go-left">
              <div class="progress">
                <div class="progress-bar progress-bar-primary go-right" role="progressbar" aria-valuenow="20"
                  aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $avgReviews->facilities * 10;?>%">
                  <span class="sr-only"></span>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-2 col-md-4 col-lg-1 go-right">
              <label class="text-left"><?php echo trans('034');?></label>
            </div>
            <div class="col-xs-9 col-md-8 col-lg-11 go-left">
              <div class="progress">
                <div class="progress-bar progress-bar-primary go-right" role="progressbar" aria-valuenow="80"
                  aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $avgReviews->staff * 10;?>%">
                  <span class="sr-only"></span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr>
        <?php } ?>
        <!-- End Hotel Reviews bars -->
       <!-- End Add/Remove Wish list Review Section -->
       
      </div>

