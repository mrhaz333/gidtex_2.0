<form autocomplete="off" action="<?php echo base_url() . $module; ?>/search" method="GET" role="search">

    <div class="go-text-right">

        <div class="col-md-4 form-group go-right col-xs-12">

            <div class="row">

<!--                <input type="text" id="myInputEle"/>-->

<!--                <script>-->

<!--                $(document).ready(function() {-->

<!--                $('#myInputEle').select2({-->

<!--                width: '100%',-->

<!--                allowClear: true,-->

<!--                multiple: true,-->

<!--                maximumSelectionSize: 1,-->

<!--                placeholder: "Start typing",-->

<!--                data: [-->

<!--                { id: 1, text: "Nikhilesh"},-->

<!--                { id: 2, text: "Raju"    }-->

<!--                ]-->

<!--                });-->

<!--                });-->

<!--                </script>-->

                <div class="clearfix"></div>

                <i class="iconspane-lg icon_set_1_icon-41"></i>

                <input type="text" data-module="<?php echo $module; ?>"

                       class="hotelsearch locationlist<?php echo $module; ?>"

                       placeholder="<?php if ($module == 'hotels') {

                           echo trans('026');

                       } elseif ($module == 'tours') {

                           echo trans('0526');

                       } ?>" value="<?php echo $_GET['txtSearch']; ?>">

                <input type="hidden" id="txtsearch" name="txtSearch" value="<?php echo $_GET['txtSearch']; ?>">

            </div>

        </div>

    </div>

    <div id="<?php if ($module == 'hotels') {

        echo 'dpd1';

    } elseif ($module == 'ean') {

        echo 'dpean1';

    } elseif ($module == 'tours') {

        echo 'tchkin';

    } ?>" class="col-md-3 form-group go-right col-xs-12 focusDateInput btn-tooltip" title="Когда потребуется техника?">



        <div class="row">

            <div class="clearfix"></div>

            <i class="iconspane-lg icon_set_1_icon-53" ></i>

            <input type="text"

                   name="<?php if ($module == 'hotels' || $module == 'ean') {

                       echo 'checkin';

                   } elseif ($module == 'tours') {

                       echo 'date';

                   } ?>" class="form input-lg <?php if ($module == 'hotels') {

                echo 'dpd1';

            } elseif ($module == 'ean') {

                echo 'dpean1';

            } elseif ($module == 'tours') {

                echo 'tchkin';

            } ?>" value="<?php if ($module == 'ean') {

                echo $themeData->eancheckin;

            } else {

                echo $themeData->checkin;

            } ?>" required>

        </div>

    </div>

    <!--    <div class="col-md-2 form-group go-right col-xs-12">

       <div class="row">

            <div class="clearfix"></div>

            <select name="adults" id="adults" class="input-lg form selectx">

                <option value="1">1 <?php /*echo trans('0446'); */?></option>

                <option value="2" selected>2 <?php /*echo trans('0446'); */?></option>

                <option value="3">3 <?php /*echo trans('0446'); */?></option>

                <option value="4">4 <?php /*echo trans('0446'); */?></option>

                <option value="5">5 <?php /*echo trans('0446'); */?></option>

            </select>

        </div>

    </div>-->

    <div class="col-md-2 form-group go-right col-xs-12">

        <div class="row">

            <!--<label class="hidden-xs go-right"><?php echo trans('0222'); ?> </label>-->

            <div class="clearfix"></div>

            <i class="iconspane-lg">

                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"

                     viewBox="0 0 511 511" width="30" height="30" xml:space="preserve">

                    <path fill="#ff5e2d" d="M47.5,428c-1.97,0-3.91,0.8-5.3,2.2c-1.4,1.39-2.2,3.32-2.2,5.3c0,1.97,0.8,3.91,2.2,5.3c1.39,1.4,3.33,2.2,5.3,2.2

                        c1.97,0,3.91-0.8,5.3-2.2c1.4-1.39,2.2-3.33,2.2-5.3c0-1.98-0.8-3.91-2.2-5.3C51.41,428.8,49.47,428,47.5,428z"/>

                    <path fill="#ff5e2d" d="M255.5,443c1.97,0,3.91-0.8,5.3-2.2c1.4-1.39,2.2-3.33,2.2-5.3c0-1.97-0.8-3.91-2.2-5.3c-1.39-1.4-3.33-2.2-5.3-2.2

                    c-1.97,0-3.91,0.8-5.3,2.2c-1.4,1.39-2.2,3.33-2.2,5.3c0,1.97,0.8,3.91,2.2,5.3C251.59,442.2,253.53,443,255.5,443z"/>

                    <path fill="#ff5e2d" d="M64,435.5c0,4.142,3.358,7.5,7.5,7.5h160c4.142,0,7.5-3.358,7.5-7.5s-3.358-7.5-7.5-7.5h-160

                    C67.358,428,64,431.358,64,435.5z"/>

                    <path fill="#ff5e2d" d="M494.46,103.214C504.301,97.88,511,87.458,511,75.5c0-17.369-14.131-31.5-31.5-31.5c-12.048,0-22.532,6.802-27.829,16.764

                    L49.391,169.07c-6.145,1.654-11.162,5.686-14.204,10.93H23.5c-4.142,0-7.5,3.358-7.5,7.5s3.358,7.5,7.5,7.5H32v9H15.5

                    C6.953,204,0,210.953,0,219.5v152c0,8.547,6.953,15.5,15.5,15.5H80v17H47.5C30.131,404,16,418.131,16,435.5S30.131,467,47.5,467

                    h208c17.369,0,31.5-14.131,31.5-31.5S272.869,404,255.5,404H223v-17h0.5c8.547,0,15.5-6.953,15.5-15.5v-12.894l112.675-112.676

                    l54.671,101.531c-3.932,4.203-6.346,9.842-6.346,16.038v16c0,0.899,0.166,1.757,0.456,2.555C395.215,387.683,392,395.221,392,403.5

                    v8c0,17.369,14.131,31.5,31.5,31.5h31.827c0.056,0.001,0.113,0.002,0.169,0.002c0.057,0,0.113,0,0.169-0.002H471.5

                    c4.142,0,7.5-3.358,7.5-7.5s-3.358-7.5-7.5-7.5h-10.343L447,378.449v-6.294L494.46,103.214z M415,363.5c0-4.687,3.813-8.5,8.5-8.5

                    s8.5,3.813,8.5,8.5v8.5h-8.5c-2.945,0-5.794,0.415-8.5,1.175V363.5z M348.171,228.223c-0.016,0.016-0.033,0.032-0.049,0.049

                    L239,337.394v-21.453l2.985-25.372c1.028-0.361,1.996-0.944,2.818-1.766l79.816-79.815c0.016-0.016,0.033-0.032,0.049-0.049

                    l23.573-23.573c0.016-0.016,0.033-0.032,0.049-0.049L449.27,84.336c3.009,10.275,11.119,18.385,21.394,21.394L348.171,228.223z

                     M135,308V195.5c0-4.687,3.813-8.5,8.5-8.5h95.566l-2,17H167.5c-8.547,0-15.5,6.953-15.5,15.5V308H135z M167,219.5

                    c0-0.276,0.224-0.5,0.5-0.5h67.801l-10.471,89H167V219.5z M321.114,191.28l-17.097-31.75c-1.077-2-1.311-4.298-0.659-6.472

                    c0.652-2.174,2.111-3.963,4.105-5.037c4.133-2.221,9.299-0.672,11.519,3.449l14.68,27.262L321.114,191.28z M479.5,59

                    c9.098,0,16.5,7.402,16.5,16.5S488.598,92,479.5,92S463,84.598,463,75.5S470.402,59,479.5,59z M47,191.762

                    c0-3.835,2.586-7.211,6.29-8.208L430.357,82.036l-85.622,85.622l-12.546-23.3c-6.137-11.398-20.414-15.683-31.83-9.549

                    c-5.528,2.975-9.566,7.925-11.37,13.938c-1.803,6.013-1.156,12.368,1.82,17.894l19.23,35.711l-65.368,65.368L254.169,187h9.331

                    c4.142,0,7.5-3.358,7.5-7.5s-3.358-7.5-7.5-7.5h-15.904c-0.07-0.001-0.14-0.001-0.21,0H143.5c-12.958,0-23.5,10.542-23.5,23.5V228

                    H82.606L65.49,210.883C61.051,206.444,55.149,204,48.873,204H47V191.762z M272,435.5c0,9.098-7.402,16.5-16.5,16.5h-208

                    c-9.098,0-16.5-7.402-16.5-16.5S38.402,419,47.5,419h208C264.598,419,272,426.402,272,435.5z M208,404H95v-17h113V404z M223.5,372

                    h-208c-0.276,0-0.5-0.224-0.5-0.5V355h16.5c4.142,0,7.5-3.358,7.5-7.5s-3.358-7.5-7.5-7.5H15V219.5c0-0.276,0.224-0.5,0.5-0.5

                    h33.373c2.271,0,4.405,0.884,6.011,2.49l6.51,6.51H47.5c-4.142,0-7.5,3.358-7.5,7.5s3.358,7.5,7.5,7.5H120v72.5

                    c0,4.142,3.358,7.5,7.5,7.5H224v17H63.5c-4.142,0-7.5,3.358-7.5,7.5s3.358,7.5,7.5,7.5H224v0.498c0,0.001,0,0.003,0,0.005V371.5

                    C224,371.776,223.776,372,223.5,372z M475.98,121.627l-39.257,222.458C432.953,341.509,428.4,340,423.5,340

                    c-1.348,0-2.667,0.12-3.953,0.339l-56.798-105.482L475.98,121.627z M423.5,428c-9.098,0-16.5-7.402-16.5-16.5v-8

                    c0-9.098,7.402-16.5,16.5-16.5h10.343l11.714,41H423.5z"/>

                </svg>

            </i>

            <select class="input-lg form selectx" name="type" id="tourtype" title="Какая техника?">

                <optgroup label="Экскаватор">

                    <option value="">Гусеничный</option>

                    <option value="">Колесный</option>

                </optgroup>

                <optgroup label="Автокраны">

                    <option value="">От 14т.</option>

                    <option value="">От 25т.</option>

                    <option value="">От 35т.</option>

                    <option value="">От 50т.</option>

                </optgroup>

                <?php foreach ($data['moduleTypes'] as $ttype) { ?>

                    <option value="<?php echo $ttype->id; ?>" <?php makeSelected($tourType, $ttype->id); ?> >

                        <?php echo $ttype->name; ?>

                    </option>

                <?php } ?>

            </select>

        </div>

    </div>

    <div class="col-md-3 form-group go-right col-xs-12 search-button">

        <div class="clearfix"></div>

        <button type="submit" class="btn-primary btn btn-lg btn-block pfb0 loader"><i class="icon_set_1_icon-66"></i>

            <?php echo trans('012'); ?> </button>

    </div>

    <input type="hidden" name="searching" class="searching" value="<?php echo $_GET['searching']; ?>">

    <input type="hidden" class="modType" name="modType" value="<?php echo $_GET['modType']; ?>">

    <script>

        $(function () {

            $(".locationlist<?php echo $module; ?>").select2({

                width: '100%',

                allowClear: true,

                maximumSelectionSize: 1,

                placeholder: "Start typing",

                data: JSON.parse('<?=$data['defaultToursListForSearchField']?>')

            }).select2("val", <?=@$_GET['searching']?>);



            $(".locationlist<?php echo $module; ?>").on("select2-open",

                function (e) {

                    $(".select2-drop-mask");

                    $(".formSection").trigger("click")

                });

            $(".locationlist<?php echo $module; ?>").on("select2-selecting", function (e) {

                $(".modType").val(e.object.module);

                $(".searching").val(e.object.id);

                $("#txtsearch").val(e.object.text);

            })

        })

    </script>

</form>

<!------------------------------------------------------------------->

<!-- ********************    TOURS MODULE    ********************  -->

<!------------------------------------------------------------------->