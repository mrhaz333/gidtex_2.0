<style>
    .tabs{
    margin-top: 120px
    }
    .tour_margin{
    margin-left: 20px;
    margin-right: 20px;
    }
    @media (max-width: 480px){.tabs{margin-top: 35px !important;}}
    @media (max-width: 624px){.tour_margin{margin-left: 0px !important; margin-right: 0px !important}}
    .form {
    padding-left: 12px !important;
    }
</style>
<?php if($_SERVER['REQUEST_URI'] == "/supplier-register/" ){ $lang_rus = true;}else{$lang_rus = false;} ?>
    <div id="Carousel" class="carousel slide carousel-fade">
        <div class="carousel-inner fadeIn animated">
            <div class="item home-slider hero active" style="background-image:url(<?php echo $theme_url; ?>assets/images/bg.jpg);background-size:cover;background-position-y:86%">
                <div class="clearfix"></div>
                <div class="container">
                    <div class="carousel-caption text-center">
                        <h1 style="text-shadow: 0 7px 11px rgba(0,0,0,.5);padding: 0px"><?php echo trans('0192');?></h1>
                        <div class="clearfix"></div>
                        <h4 style="text-shadow: 0 2px 6px rgba(0,0,0,2.5);color:#fff;"><?php echo trans('0193');?></h4>
                        <div class="clearfix"></div>
                        <div class="form-group tabs">
                            <?php  if(isModuleActive('hotels')){ ?><button id="hotels" class="btnz btn btn-default btn-lg showform"><i class="icon-building"></i> <?php echo trans('0405');?></button><?php } ?>
                            <?php  if(isModuleActive('tours')){ ?><button id="tours" class="btnz btn btn-default btn-lg showform tour_margin"><i class="icon-slideshare"></i> <?php if($lang_rus){ echo 'Регистрация';}else{echo trans('0271');}?></button><?php } ?>
                            <?php  if(isModuleActive('cars')){ ?><button  id="cars" class="btnz btn btn-default btn-lg showform"><i class="fa fa-cab"></i> <?php echo trans('0198');?></button><?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="container supplier-register-content">
    <div class="panel-body" id="apply">
        <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
            <div class="col-xs-14 col-sm-14 col-md-14 col-lg-14">
                <form class="panel panel-default" action="" method="POST" enctype="multipart/form-data" >
                    <div class="go-text-right panel-heading">
                        <span class="modulelabel" id="hotelslabel"><?php echo trans('0405');?></span>
                        <span class="modulelabel" id="tourslabel"> <?php if($lang_rus){ echo 'Регистрация';}else{echo trans('0271');} ?> </span>
                        <span class="modulelabel" id="carslabel"><?php echo trans('0198'); ?></span>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <?php if($lang_rus){$ooo = "Название компании";}else{;$ooo = trans('0350');} ?>
                            <label class="go-right"><?php echo $ooo;?></label>
                                <input data-original-title="<?php echo $ooo; ?>" data-toggle="tooltip" data-placement="top" class="form-control" type="text" placeholder="<?php echo $ooo;?>" name="itemname"  value="<?php echo  set_value('itemname'); ?>" required >
                        </div>
                        <div class="form-group">
                            <?php if($lang_rus){$fname = "Имя";}else{$fname = trans('090');}?>
                            <label class="go-right"><?php echo $fname;?> </label>
                                <input data-original-title="<?php echo $fname;?>" data-toggle="tooltip" data-placement="top" class="form-control" type="text" placeholder="<?php echo $fname;?>" name="fname"  value="<?php echo set_value('fname'); ?>" required >
                        </div>
                        <div class="form-group">
                            <?php if($lang_rus){$lname = "Фамилия";}else{$lname = trans('090');}?>
                            <label class="go-right"><?php echo $lname;?></label>
                                <input data-original-title="<?php echo $lname ;?>" data-toggle="tooltip" data-placement="top" class="form-control" type="text" placeholder="<?php echo $lname ;?>" name="lname" value="<?php echo set_value('lname'); ?>" required >
                        </div>
                        <hr class="soften">
                        <div class="form-group">
                            <label class="go-right"><?php echo trans('094');?> </label>
                                <input data-original-title="<?php echo trans('094');?>" data-toggle="tooltip" data-placement="top" class="form-control" type="email" placeholder="<?php echo trans('094');?>" name="email" value="<?php echo set_value('email'); ?>" required >
                        </div>
                        <hr class="soften">
                        <div class="form-group">
                            <label class="go-right"><?php echo trans('092');?></label>
                                <input data-original-title="<?php echo trans('092');?>" data-toggle="tooltip" data-placement="top" class="form-control" type="text" placeholder="<?php echo trans('092');?>" name="mobile" value="<?php echo set_value('mobile'); ?>" >
                        </div>
                        <div class="form-group">
                            <?php if($lang_rus){$country = "Город";}else{$country = trans('0105');}?>
                            <label class="go-right"><?php echo $country;?></label>
                                <select data-original-title="<?php echo $country;?>" data-toggle="tooltip" data-placement="top" data-placeholder="Select" name="country" class="form-control"  required>
                                    <option value=""> <?php echo 'Выберите город';?> </option>
                                    <?php foreach($allcountries as $c){ ?>
                                    <option value="<?php echo $c->iso2;?>"><?php echo $c->short_name;?></option>
                                    <?php } ?>
                                </select>
                        </div>
<!--                        <div class="form-group">-->
<!--                            <label class="go-right">--><?php //echo trans('0101');?><!--</label>-->
<!--                                <input data-original-title="--><?php //echo trans('0101');?><!--" data-toggle="tooltip" data-placement="top" class="form-control" type="text" placeholder="--><?php //echo trans('0101');?><!--" name="state" value="--><?php //echo set_value('state'); ?><!--">-->
<!--                        </div>-->
<!--                        <div class="form-group">-->
<!--                            <label class="go-right">--><?php //echo trans('0100');?><!--</label>-->
<!--                                <input data-original-title="--><?php //echo trans('0100');?><!--" data-toggle="tooltip" data-placement="top" class="form-control" type="text" placeholder="--><?php //echo trans('0100');?><!--" name="city" value="--><?php //echo set_value('city'); ?><!--">-->
<!--                        </div>-->
                        <div class="form-group">
                            <label class="go-right"><?php echo trans('098');?></label>
                                <input data-original-title="<?php echo trans('098');?>" data-toggle="tooltip" data-placement="top" class="form-control" type="text" placeholder="<?php echo trans('098');?>" name="address1" value="<?php echo set_value('address1'); ?>">
                        </div>
                        <div class="form-group">
                            <label class="go-right"><?php echo trans('099');?></label>
                                <input data-original-title="<?php echo trans('099');?>" data-toggle="tooltip" data-placement="top" class="form-control" type="text" placeholder="<?php echo trans('099');?>" name="address2" value="<?php echo set_value('address2'); ?>">
                        </div>
                    </div>
                    <input type="hidden" name="addaccount" value="1" />
                    <input type="hidden" name="type" value="supplier" />
                    <div class="panel-footer">
                        <input type="hidden" id="applyfor" name="applyfor" value="">
                        <button  type="submit" class="btn-block btn btn-lg btn-action center-block"><?php echo trans('05');?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div id="Translation">

        <h3>Классический текст Lorem Ipsum, используемый с XVI века</h3><p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p><h3>Абзац 1.10.32 "de Finibus Bonorum et Malorum", написанный Цицероном в 45 году н.э.</h3><p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p>
        <h3>Английский перевод 1914 года, H. Rackham</h3>
        <p>"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"</p>
        <h3>Абзац 1.10.33 "de Finibus Bonorum et Malorum", написанный Цицероном в 45 году н.э.</h3>
        <p>"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."</p>
        <h3>Английский перевод 1914 года, H. Rackham</h3>
        <p>"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."</p>
    </div>


<!--    --><?php //if(!empty($success)){   ?>
<!--    <div class="alert alert-success">-->
<!--        <i class="fa fa-check"></i>-->
<!--        --><?php // echo trans('0244');  ?>
<!--    </div>-->
<!--    --><?php //  }else{
//        if(!empty($error)){  ?>
<!--    <div class="alert alert-danger">-->
<!--        --><?php // echo @$error;  ?>
<!--    </div>-->
<!--    --><?php //} } ?>
<!--    <h2 class="text-center h1-title">--><?php //echo $app_settings[0]->site_title;?><!-- --><?php //echo trans('0494');?><!--</h2>-->
<!--    <hr class="hrs">-->
<!--    <br><br>-->
<!--    <div class="col-md-6 wow fadeInRight animated">-->
<!--        <div class="col-md-2 icons_right"><i class="pull-left icon_set_2_icon-105 icons"></i></div>-->
<!--        <div class="col-md-7">-->
<!--            <h3 class="text-right"> --><?php //echo trans('0495');?><!--</h3>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="col-md-6 wow fadeInLeft animated">-->
<!--        <div class="col-md-2">&nbsp;</div>-->
<!--        <div class="col-md-2"><i class="pull-right icon_set_1_icon-12 icons"></i></div>-->
<!--        <div class="col-md-7">-->
<!--            <h3 class="text-left"> --><?php //echo trans('0496');?><!--</h3>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="clearfix"></div>-->
<!--    <br>-->
<!--    <div class="col-md-6 wow fadeInRight animated">-->
<!--        <div class="col-md-2 icons_right"><i class="pull-left icon_set_1_icon-18 icons"></i></div>-->
<!--        <div class="col-md-7">-->
<!--            <h3 class="text-right"> --><?php //echo trans('0497');?><!--</h3>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="col-md-6 wow fadeInLeft animated">-->
<!--        <div class="col-md-2">&nbsp;</div>-->
<!--        <div class="col-md-2"><i class="pull-right icon_set_1_icon-30 icons"></i></div>-->
<!--        <div class="col-md-7">-->
<!--            <h3 class="text-left"> --><?php //echo trans('0498');?><!--</h3>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="clearfix"></div>-->
<!--    <br>-->
<!--    <div class="col-md-6 wow fadeInRight animated">-->
<!--        <div class="col-md-2 icons_right"><i class="pull-left icon_set_1_icon-35 icons"></i></div>-->
<!--        <div class="col-md-7">-->
<!--            <h3 class="text-right"> --><?php //echo trans('0499');?><!--</h3>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="col-md-6 wow fadeInLeft animated">-->
<!--        <div class="col-md-2">&nbsp;</div>-->
<!--        <div class="col-md-2"><i class="pull-right icon_set_1_icon-63 icons"></i></div>-->
<!--        <div class="col-md-7">-->
<!--            <h3 class="text-left"> --><?php //echo trans('0500');?><!--</h3>-->
<!--        </div>-->
<!--    </div>-->
    <div class="clearfix"></div>
    <style>
        /*  .btn-default:hover, .btn-default:focus, .btn-default.focus, .btn-default:active, .btn-default.active, .open > .dropdown-toggle.btn-default {
        background-color: #FFFFFF;
        } */
        .icons {
        font-size: 60px !important;
        color: #0066FF !important;
        }
        .icons_right{
        float: right;
        margin-right: 126px;
        }
        body {
        background-color:#fff;
        }
        .btnz {
        min-width: 190px;
        border: 1px solid #fff;
        /* margin-right: 30px;*/
        height: 70px;
        background-color: rgba(0,0,0,.3);
        color: #fff;
        text-align: left;
        cursor: pointer;
        transition-duration: .5s;
        font-size:30px;
        }
        .btnz:hover {
        background-color: rgba(255, 255, 255, 0.3);
        border: 1px solid #fff;
        }
        .c-form--banner-bottom {
        position: absolute;
        bottom: 0;
        width: 100%;
        }
        .started {
        background-color: #f3f3f3;
        }
        .list {
        padding:15px 30px 15px 30px;border-radius:5px; box-shadow: 2px 2px 2px 0 rgba(44,49,55,.1)
        }
        .lapcon {
        position: absolute;
        font-size: 65px;
        margin-top: 25px;
        margin-left: 106px;
        color: #0066FF;
        }
        .hrs {
        margin-top: 14px;
        margin-bottom: 19px;
        border: 0;
        border-top: 4px solid #0066FF;
        margin-left: 35%;
        margin-right: 35%;
        }
        @media (max-width: 585px){.btnz{margin-bottom: 5px !important; width:100%;}}
        @media (max-width: 991px){.list{margin-bottom: 5px !important;}}
        @media (max-width: 1200px){.icons_right{margin-right: 110px !important;}}
        @media (max-width: 991px){.icons_right{margin-right:0px !important; float:none;}}
    </style>
</div>
<br><br>
<!--<section class="started">-->
<!--    <div class="container">-->
<!--        <h2 class="text-center">--><?php //echo trans('0502');?><!--</h2>-->
<!--        <hr class="hrs">-->
<!--        <div class="col-md-12">-->
<!--            <div class="wow fadeInDown animated">-->
<!--                <div class="step">-->
<!--                    <div class="row-fluid img-thumbnail list">-->
<!--                        <div class="col-md-4">-->
<!--                            <img class="img-responsive" src="--><?php //echo $theme_url; ?><!--assets/img/supplier/signup.png">-->
<!--                        </div>-->
<!--                        <div class="col-md-8">-->
<!--                            <h3><strong>--><?php //echo trans('0115');?><!--</strong></h3>-->
<!--                            <p>--><?php //echo trans('0504');?><!--</p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="wow fadeInDown animated">-->
<!--                <div class="step">-->
<!--                    <div class="row-fluid img-thumbnail list">-->
<!--                        <div class="col-md-4">-->
<!--                            <img class="img-responsive" src="--><?php //echo $theme_url; ?><!--assets/img/supplier/verify.png">-->
<!--                        </div>-->
<!--                        <div class="col-md-8">-->
<!--                            <h3><strong>--><?php //echo trans('0505');?><!--</strong></h3>-->
<!--                            <p>--><?php //echo trans('0507');?><!-- </p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="wow fadeInDown animated">-->
<!--                <div class="step">-->
<!--                    <div class="row-fluid img-thumbnail list">-->
<!--                        <div class="col-md-4">-->
<!--                            <img class="img-responsive" src="--><?php //echo $theme_url; ?><!--assets/img/supplier/access.png">-->
<!--                        </div>-->
<!--                        <div class="col-md-8">-->
<!--                            <h3><strong>--><?php //echo trans('0508');?><!--</strong></h3>-->
<!--                            <p>--><?php //echo trans('0510');?><!--</p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->
<!--<div class="container">
    <div class="form-group">
        <br>
        <h3 class="text-center"><a class="btn btn-action btn-block btn-lg" href="#top"> <?php /*echo trans('0511');*/?> </a></h3>
        <br><br>
    </div>
</div>-->
<script type="text/javascript">
 $(function(){
     //$("#apply").hide();
     $("#hotelslabel").hide();
     $("#tourslabel").hide();
     $("#carslabel").hide();

     var module = $('.showform').prop('id');
     $("#applyfor").val(module);
     $(".modulelabel").hide();
     $("#"+module+"label").show();

     $(".showform").on("click",function(){
         $("#apply").slideToggle();
     });
 })
</script>