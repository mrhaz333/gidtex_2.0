<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<!--Доработать -->
<meta name="description" content="<?php echo @$metadescription; ?>">
<meta name="keywords" content="<?php echo @$metakeywords; ?>">
<meta name="author" content="GIDTEX">
<link rel="icon" href="<?php echo PT_GLOBAL_IMAGES_FOLDER.'favicon.png';?>" type="image/x-icon">
<link rel="shortcut icon" type="image/x-icon" href="<?php echo PT_GLOBAL_IMAGES_FOLDER.'favicon.png';?>">
<link rel="image_src" href="" type="image/jpeg">
<meta property="og:type" content="website">
<meta property="og:url" content="">
<meta property="og:title" content="">
<meta property="og:image" content="">
<meta property="og:image:secure_url" content=""/>
<meta property="og:image:type" content="image/jpeg"/>
<meta property="og:image:width" content="290"/>
<meta property="og:image:height" content="299"/>
<meta property="og:description" content="">
<meta property="og:site_name" content="">
<meta property="og:locale" content="ru_ru">
<link href="https://fonts.googleapis.com/css?family=Lora:400,700|Roboto:400,700&display=swap&subset=cyrillic" rel="stylesheet">
<title><?php echo @$pageTitle; ?></title>
<link href="https://fonts.googleapis.com/css?family=Cuprum:400,400i,700&display=swap&subset=cyrillic" rel="stylesheet">
<link href="<?php echo $theme_url; ?>assets/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="<?php echo $theme_url; ?>style.css" rel="stylesheet">
<link href="<?php echo $theme_url; ?>assets/css/navigation.css" rel="stylesheet">
<!--<link href="<?php echo $theme_url; ?>eder.css" rel="stylesheet"> -->
<link href="<?php echo $theme_url; ?>assets/css/mobile.css" rel="stylesheet" media="screen">

<!-- Google Maps --> <?php if (pt_main_module_available('ean') || $loadMap) { ?> <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=<?php echo $app_settings[0]->mapApi; ?>&libraries=places"></script> <script src="<?php echo $theme_url; ?>assets/js/infobox.js"></script><?php } ?>
<!-- jQuery --> <script src="<?php echo $theme_url; ?>assets/js/jquery-2.2.4.min.js"></script>
<!-- RTL CSS --> <?php if($isRTL == "RTL"){ ?> <link href="<?php echo $theme_url; ?>RTL.css" rel="stylesheet"> <link href="https://fonts.googleapis.com/css?family=Tajawal:300,400,500,700,800&amp;subset=arabic" rel="stylesheet"><?php } ?>
<!-- Mobile Redirect --> <?php if($mSettings->mobileRedirectStatus == "Yes"){ if($ishome != "invoice"){ ?> <script>if(/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)){ window.location ="<?php echo $mSettings->mobileRedirectUrl; ?>";}</script> <?php } } ?>
<!--[if lt IE 7] > <link rel="stylesheet" type="text/css" href="<?php echo $theme_url; ?>assets/css/font-awesome-ie7.css" media="screen" /> <![endif]-->
<!-- Autocomplete files-->
<link href="<?php echo $theme_url; ?>assets/js/autocomplete/easy-autocomplete.min.css" rel="stylesheet" type="text/css">
<script src="<?php echo $theme_url; ?>assets/js/autocomplete/jquery.easy-autocomplete.min.js" type="text/javascript" ></script>
<script src="<?php echo base_url(); ?>assets/include/datepicker/datepicker.js"></script>
<script src="<?php echo $theme_url; ?>assets/js/bootstrap.min.js"></script>
<!-- Autocomplete files-->
<script>var base_url = '<?php echo base_url(); ?>';</script>
<?php echo $app_settings[0]->google; ?>

</head>

<body>
<div id="preloader" class="loader-wrapper">
    <div class="progress">
     <div class="indeterminate"></div>
    </div>
</div>

<div class="tbar-top hidden-sm hidden-xs">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-4 col-xs-7 go-right">
                <div class="contact-box">
                    <ul class="hidden-sm hidden-xs">
                        <?php if( ! empty($phone) ) { ?>
                        <li class="go-right">
                            <span>Аренда строительной техники</span>
                            <i class="icon_set_1_icon-55 go-right align-M mrg5-R f-grey66 fs13"></i>
                            <span class="contact-no align-M">Телефон: <a href="#tel:89000000000" class="number">89000000000</a></span>
                        </li>
                        <?php } ?>
                        <?php if( ! empty($contactemail) ) { ?>
                        <li class="go-right">
                            <span class="sep go-right">|</span>
                            <i class="fa fa-envelope-o go-right align-M mrg5-R f-grey66 fs13"></i>
                            <span class="tp-mail"><a title="Mail" href="mailto:<?php echo $contactemail; ?>"><?php echo $contactemail; ?></a></span>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal <?php if($isRTL == "RTL"){ ?> right <?php } else { ?> left <?php } ?> fade" id="sidebar_left" tabindex="1" role="dialog" aria-labelledby="sidebar_left">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close go-left" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="close icon-angle-<?php if($isRTL == "RTL"){ ?>right<?php } else { ?>left<?php } ?>"></i></span></button>
                <h4 class="modal-title go-text-right" id="sidebar_left"><i class="icon_set_1_icon-65 go-right"></i> <?php echo trans('0296');?></h4>
            </div>
            <?php include 'settings.php'; ?>
        </div>
    </div>
</div>
<div id="body-section">
    <?php if ($_SERVER[REQUEST_URI] == "/"){?>
        <nav class="navbar navbar-default home-nav">
            <div class="container">
                <div class="navbar-collapse">
                    <ul class="nav navbar-nav navbar-right go-left">
                        <?php include 'settings.php'; ?>
                    </ul>
                </div>
            </div>
        </nav>
    <?}else{?>
        <nav class="navbar navbar-default other-nav">
            <div class="container" style="display: flex;">
                <div class="navbar-header go-right">
                    <button data-toggle="modal" data-target="#sidebar_left" class="navbar-toggle go-left" type="button">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="<?php echo base_url(); ?>" class="navbar-brand go-right" target="">
                        <img class="" src="<?php echo PT_GLOBAL_IMAGES_FOLDER.$app_settings[0]->header_logo_img;?>" alt="<?php echo @$pageTitle; ?>" />
                    </a>
                </div>
                <div class="collapse navbar-collapse pr0" style="margin-left: auto">
                    <ul class="nav navbar-nav go-right">
                        <!--<li class="text-center go-right"><a href="<?php echo base_url(); ?>"><?php echo trans('01');?></a></li>-->
                        <?php if($is_home != true): ?>
                            <?php include 'views/home/menu.php'; ?>
                        <?php endif; ?>
                        <?php if (isModuleActive("Blog")): ?>
                        <?php endif; ?>
                        <!--              <li class="text-center go-right"><a href="--><?php //echo base_url(); ?><!--offers">--><?php //echo trans('Offers');?><!--</a></li>-->
                    </ul>
                    <ul class="nav navbar-nav navbar-right hidden-sm go-left">
                        <?php include 'settings.php'; ?>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="clearfix"></div>
    <?php } ?>