<script>
    $(function(){
        themeinfo();
        offstatus();
        // mailserver options
        var mailserver = $("#mailserver").val();
        if(mailserver == "php"){
            $(".smtp").hide();
        }else{
            $(".smtp").show();
        }
        // mailserver options
        $("#mailserver").on('change', function() {
            var mserver = $(this).val();
            if(mserver == "php"){
                $(".smtp").hide();
            }else{
                $(".smtp").show();
            }
        });

        // offline status option
        $(".offstatus").on('change', function() {
            offstatus();

        });

        $("#hlogo").change(function(){

            var preview = $('.hlogo_preview_img');
            preview.fadeOut();

            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("hlogo").files[0]);

            oFReader.onload = function (oFREvent) {
                preview.attr('src', oFREvent.target.result).fadeIn();
            };

        });

        $("#favimage").change(function(){
            var abc = $(this).attr('name');


            var preview = $('.favimage_preview_img');
            preview.fadeOut();

            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("favimage").files[0]);

            oFReader.onload = function (oFREvent) {
                preview.attr('src', oFREvent.target.result).fadeIn();
            };

        });

        $("#wmlogo").change(function(){

            var preview = $('.wmlogo_preview_img');
            preview.fadeOut();

            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("wmlogo").files[0]);

            oFReader.onload = function (oFREvent) {
                preview.attr('src', oFREvent.target.result).fadeIn();
            };

        });

        $(".testEmail").on('click',function(){
            var id = $(".testemailtxt").val();
            $.post("<?php echo base_url();?>admin/ajaxcalls/testingEmail", {email: id}, function(resp){
                alert(resp);
                console.log(resp);
            });
        })

    });

    function themeinfo(){
        var id = $(".theme").val();

        $.post("<?php echo base_url();?>admin/ajaxcalls/ThemeInfo", {theme: id}, function(resp){
            var obj = jQuery.parseJSON(resp);

            $("#themename").html(obj.Name);
            $("#themedesc").html(obj.Description);
            $("#themeauthor").html(obj.Author);
            $("#themeversion").html(obj.Version);
            $("#screenshot").prop("src",obj.screenshot);

        });
    }

    function offstatus(){
        var status = $(".offstatus").val();
        if(status == "1"){
            $("#offmsg").prop("readonly",false);
        }else{
            $("#offmsg").prop("readonly",true);
        }
    }
</script>
<div class="container">
    <?php if(!empty($errormsg)){  echo $errormsg; } ?>
</div>
<form action="" method="POST" enctype="multipart/form-data">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title"><i class="fa fa-cogs"></i>Общие настройки</div>
            <br><br>
            <div class="panel-tools pull-right">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#GENERAL" data-toggle="tab">Основные</a></li>
                    <li class=""><a href="#SINGLE" data-toggle="tab">Сайн</a></li>
                    <li class=""><a href="#MOBILE" data-toggle="tab">Мобильная версия</a></li>
                    <li class=""><a href="#WATERMARK" data-toggle="tab">Вотермарка</a></li>
                    <li class=""><a href="#EMAIL" data-toggle="tab">Email</a></li>
                    <li class=""><a href="#THEMES" data-toggle="tab">Темы</a></li>
                    <li class=""><a href="#CONTACT" data-toggle="tab">Контакты</a></li>
                    <li class=""><a href="#SERVER" data-toggle="tab">Информация о сервере</a></li>
                </ul>
            </div>
        </div>
        <div class="panel-body">
            <div class="tab-content form-horizontal">
                <div class="tab-pane wow fadeIn animated active in" id="GENERAL">
                    <div class="well well-sm">
                        <div class="row form-group">
                            <label  class="col-md-2 control-label text-left">Логотип</label>
                            <div class="col-md-4">
                                <div class="input-group input-xs">
                                    <input type="file" class="btn btn-default" id="hlogo" name="hlogo">
                                    <span class="help-block">Только PNG</span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <img src="<?php echo PT_GLOBAL_IMAGES_FOLDER.'logo.png?id='.rand(1,99);?>"  class="hlogo_preview_img img-responsive" />
                            </div>
                        </div>
                    </div>
                    <div class="well well-sm">
                        <div class="row form-group">
                            <label  class="col-md-2 control-label text-left">Фавикон</label>
                            <div class="col-md-4">
                                <div class="input-group input-xs">
                                    <input type="file" class="btn btn-default" id="favimage" name="favimg">
                                    <span class="help-block">Только PNG</span>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <img src="<?php echo PT_GLOBAL_IMAGES_FOLDER.'favicon.png?id='.rand(1,99);?>" width="60" height="60" alt="" class="img-responsive favimage_preview_img" />
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Имя компании</label>
                        <div class="col-md-4">
                            <input name="site_title" type="text"  placeholder="Имя компании" class="form-control" value="<?php echo $settings[0]->site_title;?>" />
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">URL сайта</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" placeholder="URL сайта" name="site_url" value="<?php echo $settings[0]->site_url;?>">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Лицензия</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" placeholder="Лицензия" name="license" value="<?php echo $settings[0]->license_key;?>">
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Копирайт</label>
                        <div class="col-md-4">
                            <input name="copyright" type="text" placeholder="Копирайт" class="form-control" value="<?php echo $settings[0]->copyright; ?>"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Формат даты</label>
                        <div class="col-md-2">
                            <select data-placeholder="Выбрать" class="form-control" name="pt_date_format">
                                <option value="d/m/Y" <?php if ($settings[0]->date_f == "d/m/Y") {echo "selected";}?> >dd/mm/yyyy</option>
                                <option value="m/d/Y" <?php if ($settings[0]->date_f == "m/d/Y") {echo "selected";}?> >mm/dd/yyyy</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Мультиязычность</label>
                        <div class="col-md-2">
                            <select data-placeholder="Выбрать" class="form-control" name="multi_lang">
                                <option value="1" <?php if ($settings[0]->multi_lang == '1') {echo 'selected';}?> >Вкл</option>
                                <option value="0" <?php if ($settings[0]->multi_lang == '0') {echo 'selected';}?> >Откл</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Основной язык</label>
                        <div class="col-md-2">
                            <select data-placeholder="Выбрать" class="form-control" name="default_lang">
                                <?php
                                $language_list = pt_get_languages();
                                foreach ($language_list as $langid => $langname) {
                                    ?>
                                    <option value="<?php echo $langid;?>" <?php if ($settings[0]->default_lang == $langid) {echo 'selected';}?> ><?php echo $langname['name'];?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Мультивалюты</label>
                        <div class="col-md-2">
                            <select data-placeholder="Выбрать" class="form-control" name="multicurr">
                                <option value="1" <?php if ($settings[0]->multi_curr == "1") {echo 'selected';}?> >Вкл</option>
                                <option value="0" <?php if ($settings[0]->multi_curr == "0") {echo 'selected';}?> >Откл</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Закрыть сайт от гостей</label>
                        <div class="col-md-2">
                            <select name="restrict" class="form-control">
                                <option value="No" <?php makeSelected('No',$settings[0]->restrict_website); ?> > Нет </option>
                                <option value="Yes" <?php makeSelected('Yes',$settings[0]->restrict_website); ?> > Да </option>
                            </select>
                        </div>
                        <small><strong> Если выбрано «Да», то только зарегистрированные пользователи могут получить доступ к вашему веб-сайту.</strong></small>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Регистрация пользователей</label>
                        <div class="col-md-2">
                            <select data-placeholder="Выбрать" class="form-control" name="allow_registration">
                                <option value="1" <?php if ($settings[0]->allow_registration == "1") {echo "selected";}?> >Да</option>
                                <option value="0" <?php if ($settings[0]->allow_registration == "0") {echo "selected";}?> >Нет</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Подтверждение пользователей</label>
                        <div class="col-md-2">
                            <select data-placeholder="Выбрать" class="form-control" name="user_reg_approval">
                                <option value="Yes" <?php if ($settings[0]->user_reg_approval == "Yes") {echo "selected";}?> >Авто подтверждение</option>
                                <option value="No" <?php if ($settings[0]->user_reg_approval == "No") {echo "selected";}?> >Админ подтверждает</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Регистрация арендодателей</label>
                        <div class="col-md-2">
                            <select data-placeholder="Выбрать" class="form-control" name="allow_supplier_registration">
                                <option value="1" <?php if ($settings[0]->allow_supplier_registration == "1") {echo "selected";}?> >Да</option>
                                <option value="0" <?php if ($settings[0]->allow_supplier_registration == "0") {echo "selected";}?> >Нет</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Отзывы</label>
                        <div class="col-md-2">
                            <select data-placeholder="Select" class="form-control" name="reviews">
                                <option value="Yes" <?php if ($settings[0]->reviews == "Yes") {echo "selected";}?> >Авто подтверждение</option>
                                <option value="No" <?php if ($settings[0]->reviews == "No") {echo "selected";}?> >Админ подтверждает</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Окончание аренды</label>
                        <div class="col-md-2">
                            <input class="form-control input-sm" type="number" placeholder="Days" name="bookingexpiry" min="1" value="<?php echo $settings[0]->booking_expiry;?>">
                        </div>
                        <small><strong>Введите количество дней, в течение которых заканчивается неоплаченная аренда</strong></small>
                    </div>
                    <div class="row form-group" style="display: none;">
                        <label  class="col-md-2 control-label text-left">Купоны</label>
                        <div class="col-md-3">
                            <select data-placeholder="Select" class="form-control" name="coupon_code_type">
                                <option value="alpha" <?php if ($settings[0]->coupon_code_type == "alpha") {echo "selected";}?> >Alphabets Only</option>
                                <option value="numeric" <?php if ($settings[0]->coupon_code_type == "numeric") {echo "selected";}?> > Numbers Only </option>
                                <option value="alnum" <?php if ($settings[0]->coupon_code_type == "alnum") {echo "selected";}?> > Alphabets and Numbers both</option>
                            </select>
                        </div>
                        <small><strong>Select Coupon Code type to be generated as coupon codes </strong></small>
                    </div>
                    <div class="row form-group" style="display: none;">
                        <label  class="col-md-2 control-label text-left"> Coupon code Length </label>
                        <div class="col-md-2">
                            <input class="form-control input-sm" type="number" placeholder="" name="codelength" min="4" max="8" value="<?php echo $settings[0]->coupon_code_length;?>">
                        </div>
                        <small><strong>Enter coupon code length min: 4, max: 8  </strong></small>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Оффлайн</label>
                        <div class="col-md-2">
                            <select data-placeholder="Выбрать" class="form-control offstatus" name="site_offine">
                                <option value="1" <?php if ($settings[0]->site_offline == '1') {echo 'selected';}?> >Да</option>
                                <option value="0" <?php if ($settings[0]->site_offline == '0') {echo 'selected';}?> >Нет</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Сообщение о недоступности</label>
                        <div class="col-md-8">
                            <textarea name="offlinemsg" id="offmsg" placeholder="GIDTEX в настоящее время недоступен. Пожалуйста, посетите нас позже." class="form-control" cols="" rows="2"><?php echo $settings[0]->offline_message; ?></textarea>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Тайтл</label>
                        <div class="col-md-4">
                            <input name="slogan" type="text" placeholder="Слоган" class="form-control" value="<?php echo $settings[0]->home_title;?>" />
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Ключевые слова</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" placeholder="Ключевые слова" name="keywords" value="<?php echo $settings[0]->keywords;?>" >
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Описание</label>
                        <div class="col-md-8">
                            <textarea class="form-control" rows="2" placeholder="Описание" name="meta_description" ><?php echo $settings[0]->meta_description;?></textarea>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left"> Google Map API </label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" placeholder="Google Map API" name="mapapi" value="<?php echo $settings[0]->mapApi; ?>" >
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Счетчики аналитики</label>
                        <div class="col-md-8">
                            <textarea class="form-control" rows="4" placeholder="Вставьте свой код отслеживания и аналитики здесь" name="gacode" ><?php echo $settings[0]->google;?></textarea>
                        </div>
                    </div>
                    <!-- <div class="row form-group">
            <label  class="col-md-2 control-label text-left">Force SSL</label>
            <div class="col-md-2">
            <select data-placeholder="Select" class="form-control" name="ssl_url">
            <option value="1" <?php if ($settings[0]->ssl_url == '1') {echo 'selected';}?> >Enabled</option>
            <option value="0" <?php if ($settings[0]->ssl_url == '0') {echo 'selected';}?> >Disabled</option>
            </select>
            </div>
            </div> -->
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">RSS-канал</label>
                        <div class="col-md-2">
                            <select name="rss" class="form-control">
                                <option value="1" <?php if ($settings[0]->rss == '1') {echo 'selected';}?>>Вкл</option>
                                <option value="0" <?php if ($settings[0]->rss == '0') {echo 'selected';}?>>Выкл</option>
                            </select>
                        </div>
                        <label class="col-md-3"><a class="btn btn-success btn-block" target="_blank" href="<?php base_url(); ?>admin/settings/downloadSitemap">Скачать карту сайта</a></label>
                        <label class="col-md-3"><a class="btn btn-primary btn-block" target="_blank" href="<?php base_url(); ?>sitemap.xml">Показать карту сайта</a></label>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Обновления</label>
                        <div class="col-md-2">
                            <select name="updates" class="form-control">
                                <?php for($i = 1; $i <= 7; $i++){ $d = $i * 24; if($i > 1){ $days = "Days"; }else{ $days = "Day"; } ?>
                                    <option value="<?php echo $d;?>" <?php if ($settings[0]->updates_check == $d) {echo 'selected';}?>>Каждые <?php echo $i." ".$days; ?></option>
                                <?php } ?>
                                <option value="0" <?php if ($settings[0]->updates_check == '0') {echo 'selected';}?>>Никогда</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="tab-pane wow fadeIn animated in form-horizontal" id="SINGLE">
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Статус SPA</label>
                        <div class="col-md-2">
                            <select class="form-control" name="spa_status">
                                <option>Select Status</option>
                                <option value="enable" <?=($spa_settings->spa_status == 'enable')?'selected':''?>>Вкл</option>
                                <option value="disable" <?=($spa_settings->spa_status == 'disable')?'selected':''?>>Выкл</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <p style="padding: 5px;">Включение SPA</p>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Имя модуля</label>
                        <div class="col-md-2">
                            <select class="form-control" name="spa_module">
                                <option>Выбрать модуль</option>
                                <option value="tours" <?=($spa_settings->spa_module == 'tours')?'selected':''?>>Аренда</option>
                            </select>
                        </div>
                        <div class="col-md-8">
                            <select class="chosen-multi-select" data-placeholder="my placeholder"  name="spa_homepage"><?=$spa_homepage_dd?></select>
                        </div>
                    </div>
                    <script>
                        $('[name=spa_module]').change(function(){
                            var payload = { module: $(this).val() };
                            $.get('<?=base_url("suggestions/spaAutoComplete")?>',payload,function(res){
                                // var res=JSON.parse(res);
                                $('[name=spa_homepage]').html(res.html);
                            });
                        });
                    </script>
                </div>
                <div class="tab-pane wow fadeIn animated in" id="MOBILE">
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">API ключ</label>
                        <div class="col-md-5">
                            <input name="mobile[apiKey]" type="text"  placeholder="API ключ" class="form-control" value="<?php echo $mobileSettings->apiKey;?>" />
                        </div>
                        <p>Ссылка доступа к API: <a target="_blank" href="<?php echo base_url(); ?>api/"><strong><?php echo base_url(); ?>api/</strong></a> </p>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Мобильные версии</label>
                        <div class="col-md-2">
                            <select data-placeholder="Выбор" class="form-control" name="mobile[mobileSectionStatus]">
                                <option value="Yes" <?php makeSelected('Yes',$mobileSettings->mobileSectionStatus); ?> >Вкл</option>
                                <option value="No" <?php makeSelected('No',$mobileSettings->mobileSectionStatus); ?> > Откл</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="mobile[iosUrl]" placeholder="iOS Store URL" value="<?php echo $mobileSettings->iosUrl;?>" />
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="mobile[androidUrl]" placeholder="Android Store URL" value="<?php echo $mobileSettings->androidUrl;?>" />
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Редирект мобильных пользователей</label>
                        <div class="col-md-2">
                            <select data-placeholder="Select" class="form-control" name="mobile[mobileRedirectStatus]">
                                <option value="Yes" <?php makeSelected('Yes',$mobileSettings->mobileRedirectStatus); ?> >Вкл</option>
                                <option value="No" <?php makeSelected('No',$mobileSettings->mobileRedirectStatus); ?> > Выкл</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="mobile[mobileRedirectUrl]" placeholder="URL" value="<?php echo @$mobileSettings->mobileRedirectUrl;?>" />
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">О проекте</label>
                        <div class="col-md-10">
                            <textarea class="form-control" rows="10"  name="mobile[aboutUs]" placeholder="About Us" ><?php echo @$mobileSettings->aboutUs;?></textarea>
                        </div>
                    </div>
                </div>
                <div class="tab-pane wow fadeIn animated in" id="WATERMARK">
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Вкл</label>
                        <div class="col-md-2">
                            <select data-placeholder="Выбрать" class="form-control" name="wm_status">
                                <option value="1" <?php echo makeSelected("1",$wm_settings[0]->wm_status); ?> >Да</option>
                                <option value="0" <?php echo makeSelected("0",$wm_settings[0]->wm_status); ?> >Нет</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Позиция</label>
                        <div class="col-md-2">
                            <select data-placeholder="Выбрать" class="form-control" name="img_position">
                                <option value="tr" <?php echo makeSelected("tr",$wm_settings[0]->wm_position); ?> >Вверх справа</option>
                                <option value="tl" <?php echo makeSelected("tl",$wm_settings[0]->wm_position); ?> >Вверх слева</option>
                                <option value="tc" <?php echo makeSelected("tc",$wm_settings[0]->wm_position); ?> >Вверх центр</option>
                                <option value="br" <?php echo makeSelected("br",$wm_settings[0]->wm_position); ?> >Низ справо</option>
                                <option value="bl" <?php echo makeSelected("bl",$wm_settings[0]->wm_position); ?> >Низ слева</option>
                                <option value="bc" <?php echo makeSelected("bc",$wm_settings[0]->wm_position); ?> >Низ центр</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Изображение</label>
                        <div class="col-md-4">
                            <div class="input-group input-xs">
                                <input type="file" class="btn btn-default" id="wmlogo" name="wmimg">
                                <span class="help-block">Только PNG</span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <img src="<?php echo PT_GLOBAL_IMAGES_FOLDER.'watermark.png?id='.rand(1,99);?>"  class="wmlogo_preview_img img-responsive" />
                        </div>
                    </div>
                </div>
                <div class="tab-pane wow fadeIn animated in" id="EMAIL">
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Отпрака почты</label>
                        <div class="col-md-2">
                            <select name="defmailer" class="form-control" id="mailserver">
                                <option value="php" <?php if( $mailserver[0]->mail_default == "php"){ echo "selected"; } ?> >PHP Mailer</option>
                                <option value="smtp" <?php if( $mailserver[0]->mail_default == "smtp"){ echo "selected"; } ?>   >SMTP</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Email</label>
                        <div class="col-md-4">
                            <input type="email" name="fromemail" placeholder="Email" class="form-control" value="<?php echo $mailserver[0]->mail_fromemail;?>" />
                        </div>
                    </div>
                    <hr>
                    <div class="smtp">
                        <div class="row form-group">
                            <label  class="col-md-2 control-label text-left">Защищеный SMTP</label>
                            <div class="col-md-2">
                                <select name="smtpsecure" class="form-control">
                                    <option value="ssl" <?php if( $mailserver[0]->mail_secure == "ssl"){ echo "selected"; } ?>>SSL</option>
                                    <option value="tls" <?php if($mailserver[0]->mail_secure == "tls"){ echo "selected"; } ?> >TLS</option>
                                    <option value="no" <?php if($mailserver[0]->mail_secure == "no"){ echo "selected"; } ?> >Нет</option>
                                </select>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label  class="col-md-2 control-label text-left">SMTP хост</label>
                            <div class="col-md-4">
                                <input type="text" name="smtphost" placeholder="Host" class="form-control" value="<?php echo $mailserver[0]->mail_hostname;?>" />
                            </div>
                        </div>
                        <div class="row form-group">
                            <label  class="col-md-2 control-label text-left">SMTP порт</label>
                            <div class="col-md-2">
                                <input type="text" name="smtpport" placeholder="Port" value="<?php echo $mailserver[0]->mail_port;?>" class="form-control"/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label  class="col-md-2 control-label text-left">SMTP логин</label>
                            <div class="col-md-4">
                                <input type="text" name="smtpuser" placeholder="Username" value="<?php echo $mailserver[0]->mail_username;?>" class="form-control"/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label  class="col-md-2 control-label text-left">SMTP пароль</label>
                            <div class="col-md-4">
                                <input type="text" name="smtppass" placeholder="password" value="<?php echo $mailserver[0]->mail_password;?>" class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left">Тест почты</label>
                        <div class="col-md-4">
                            <input type="text" name="" placeholder="Email" value="" class="form-control testemailtxt"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left "><br></label>
                        <div class="col-md-4">
                            <span class="btn btn-sm btn-primary testEmail">Отправить</span>
                        </div>
                    </div>
                    <hr>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left ">Заголовок письма</label>
                        <div class="col-md-10">
                            <textarea name="mailheader" class="form-control" rows="4" cols="100"><?php echo $mailserver[0]->mail_header;?></textarea>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label  class="col-md-2 control-label text-left ">Окончание письма</label>
                        <div class="col-md-10">
                            <textarea name="mailfooter" class="form-control" rows="4" cols="100"><?php echo $mailserver[0]->mail_footer;?></textarea>
                        </div>
                    </div>
                </div>
                <div class="tab-pane wow fadeIn animated in" id="THEMES">
                    <div class="row form-group">
                        <label  class="col-md-1 control-label text-left">Темы</label>
                        <div class="col-md-2">
                            <select name="theme" class="form-control theme">
                                <?php foreach($themes as $theme => $v )
                                {     @$themeinfo = pt_getThemeInfo( "themes/$theme/style.css" );
                                    ?>
                                    <option value="<?php echo $theme;?>" <?php if($settings[0]->default_theme == $theme){ echo "selected"; } ?> ><?php echo $themeinfo['Name']; ?></option>
                                <?php  } ?>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <img id="screenshot" src="" class="img-responsive img-thumbnail" alt="themes" />
                        </div>
                        <div class="col-md-6">
                            <p><strong>Имя темы :</strong> <span id="themename"></span></p>
                            <p><strong>Описание :</strong> <span id="themedesc"></span></p>
                            <p><strong>Автор :</strong> <span id="themeauthor"></span></p>
                            <p><strong>Версия :</strong> <span id="themeversion"></span></p>
                        </div>
                    </div>
                </div>
                <div class="tab-pane wow fadeIn animated in" id="CONTACT">
                    <div class="panel-body">
                        <div class="row form-group">
                            <label class="col-md-2 control-label text-left">Номер телефона</label>
                            <div class="col-md-4">
                                <input class="form-control input-sm" type="text" placeholder="Phone Number" name="contact_phone" value="<?php echo $contact_data[0]->contact_phone;?>">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-2 control-label text-left">Email</label>
                            <div class="col-md-4">
                                <input class="form-control input-sm" type="text" placeholder="Email address" name="contact_email" value="<?php echo $contact_data[0]->contact_email;?>">
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-2 control-label text-left">Адрес</label>
                            <div class="col-md-6">
                                <textarea cols="20" rows="5" type="text" class="form-control" placeholder="Office Address" name="contact_address"  /><?php echo $contact_data[0]->contact_address;?></textarea>
                            </div>
                        </div>
                        <input type="hidden" name="contact_page_id" value="<?php echo $contact_data[0]->contact_id;?>">
                    </div>
                </div>
                <div class="tab-pane wow fadeIn animated in" id="SERVER">
                    <div class="list-group">
                        <a href="" class="list-group-item"><strong> Операционная система </strong> <span  class="pull-right"><?php echo info_general('os');?></span></a>
                        <a href="" class="list-group-item"><strong> Браузер </strong> <span  class="pull-right"><?php echo $browserlib->getBrowser()." ".$browserlib->getVersion() ?> </span></a>
                        <a data-toggle="modal" href="#phpinfo" class="list-group-item"><strong> PHP Version </strong> <span  class="pull-right"><?php echo phpversion(); echo phpversion('tidy'); ?></span></a>
                        <a href="" class="list-group-item"><strong> MySQL  </strong> <span  class="pull-right"><?php echo info_general('mysqlversion');?></span></a>
                        <a href="" class="list-group-item"><strong> MySQLi </strong> <span  class="pull-right"> <?php $mysqli = info_general('mysqli'); if($mysqli){ ?><i class='btn btn-success btn-xs glyphicon glyphicon-ok'></i><?php }else{?><i class='btn btn-danger btn-xs glyphicon glyphicon-remove'></i> <?php } ?> </span></a>
                        <a href="" class="list-group-item"><strong> Mod_Rewrite </strong> <span  class="pull-right"> <?php $modrewrite = info_general('modrewrite'); if($modrewrite){ ?><i class='btn btn-success btn-xs glyphicon glyphicon-ok'></i><?php }else{?><i class='btn btn-danger btn-xs glyphicon glyphicon-remove'></i> <?php } ?> </span></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <input type="hidden" name="globalsettings" value="1"/>
            <button class="btn btn-primary">Сохранить</button>
        </div>
    </div>
</form>