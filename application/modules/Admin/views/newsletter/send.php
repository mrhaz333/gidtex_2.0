<script>
  $(function(){

        slideout();
           // Remove selected subscriber
          $('.del_selected').click(function(){
      var subscribers = new Array();
      $("input:checked").each(function() {
           subscribers.push($(this).val());
        });
      var count_checked = $("[name='news_ids[]']:checked").length;
      if(count_checked == 0) {
        $.alert.open('info', 'Пожалуйста, выберите подписчика для удаления.');
        return false;
         }


  $.alert.open('confirm', 'Вы уверены, что хотите это удалить', function(answer) {
     if (answer == 'yes')


         $.post("<?php echo base_url();?>admin/ajaxcalls/delete_multiple_subscribers", { newslist: subscribers }, function(theResponse){

                    location.reload();


  	});


  });

    });


    // disable selected Subscriber
        $('.disable_selected').click(function(){
      var subscribers = new Array();
      $("input:checked").each(function() {
           subscribers.push($(this).val());
        });
      var count_checked = $("[name='news_ids[]']:checked").length;
      if(count_checked == 0) {
       $.alert.open('info', 'Пожалуйста, выберите подписчика, чтобы отключить.');
        return false;
         }

    $.alert.open('confirm', 'Вы уверены, что хотите отключить его', function(answer) {
        if (answer == 'yes')


           $.post("<?php echo base_url();?>admin/ajaxcalls/disable_multiple_subscribers", { newslist: subscribers }, function(theResponse){

                    location.reload();


  	});


    });


    });

        // enable selected subscriber
        $('.enable_selected').click(function(){
      var subscribers = new Array();
      $("input:checked").each(function() {
           subscribers.push($(this).val());
        });
      var count_checked = $("[name='news_ids[]']:checked").length;
      if(count_checked == 0) {
        $.alert.open('info', 'Пожалуйста, выберите подписчика для включения.');
        return false;
         }

    $.alert.open('confirm', 'Вы уверены, что хотите включить его', function(answer) {
        if (answer == 'yes')


          $.post("<?php echo base_url();?>admin/ajaxcalls/enable_multiple_subscribers", { newslist: subscribers }, function(theResponse){

                    location.reload();


  	});


    });


    });

    // Enable single Subscriber

    $(".enable_single").click(function(){
   var id = $(this).attr('id');


   $.alert.open('confirm', 'Вы уверены, что хотите включить его', function(answer) {
       if (answer == 'yes')


            $.post("<?php echo base_url();?>admin/ajaxcalls/enable_single_subscriber", { newsid: id }, function(theResponse){

                    location.reload();


  	});


   });

    });

        // Disable single Subscriber

    $(".disable_single").click(function(){
   var id = $(this).attr('id');


  $.alert.open('confirm', 'Вы уверены, что хотите отключить его', function(answer) {
     if (answer == 'yes')


          $.post("<?php echo base_url();?>admin/ajaxcalls/disable_single_subscriber", { newsid: id }, function(theResponse){

                    location.reload();


  	});


  });

    });

       //delete single subscriber
    $(".del_single").click(function(){
   var id = $(this).attr('id');


  $.alert.open('confirm', 'Вы уверены, что хотите это удалить', function(answer) {
     if (answer == 'yes')


   $.post("<?php echo base_url();?>admin/ajaxcalls/delete_single_subscriber", { newsid: id }, function(theResponse){

                    location.reload();


  	});


  });

    });


  })


</script>
  <form class="form-horizontal" action="" method="POST">
          <div class="panel panel-default">
          <div class="panel-heading">
              Отправить
              </div>
            <div class="panel-body">

                <fieldset>
                  <div class="form-group">
                    <label class="col-md-1 control-label">Отправить</label>
                    <div class="col-md-3">
                      <select data-placeholder="Select" class="chosen-select" name="sendto">
                        <option value="everyone"> Всем</option>
                        <option value="subscribers"> Подписчикам</option>
                        <option value="admin"> Админам</option>
                        <option value="supplier"> Владельцам</option>
                        <option value="customers"> Клиентам</option>
                        <option value="guest"> Гостям</option>
                     </select>
                    </div>
                    <label class="col-md-1 control-label">Тема</label>
                    <div class="col-md-5">
                      <input class="form-control" type="text" placeholder="Тема рассылки" name="subject">
                    </div>

                    <div class="col-md-2">
          <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-envelope"></i> Отправить</button>
                    </div>



                  </div>
                </fieldset>
            </div>
          </div>


          <textarea class="ckeditor" cols="80" id="editor"  rows="10" name="content">  </textarea>

          <input type="hidden" name="sendnews" value="1" />

        </form>


<style>
.cke_contents { height: 375px !important; }
</style>