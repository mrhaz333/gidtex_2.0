<style type="text/css">
  .modal .modal-body {
    max-height: 450px;
    overflow-y: auto;
}
</style>

<?php if ($this->session->flashdata('flashmsgs')) {echo NOTIFY;}?>
<h3 class="margin-top-0">Настройки</h3>
<form action="" method="POST">
  <div class="panel panel-default">
    <ul class="nav nav-tabs nav-justified" role="tablist">
      <li class="active" id="Generaltab"><a href="#GENERAL" data-toggle="tab">Основное</a></li>
      <li class=""><a href="#TOURTYPES" data-toggle="tab">Типы</a></li>
      <li class=""><a href="#INCLUSIONS" data-toggle="tab">Дополнительные услуги</a></li>
      <li class=""><a href="#EXCLUSIONS" data-toggle="tab">Исключения</a></li>
      <li class=""><a href="#PAYMENT" data-toggle="tab">Способы оплаты</a></li>
    </ul>
    <div class="panel-body">
      <br>
      <div class="tab-content form-horizontal">
        <div class="tab-pane wow fadeIn animated active in" id="GENERAL">
          <div class="clearfix"></div>
          <!--<div class="row form-group">
            <label class="col-md-2 control-label text-left">Icon Class</label>
            <div class="col-md-4">
              <input type="text" name="page_icon" class="form-control" placeholder="Select icon" value="<?php echo $settings[0]->front_icon;?>" >
            </div>
          </div>-->
          <div class="row form-group">
            <label class="col-md-2 control-label text-left">Тип ссылки</label>
            <div class="col-md-4">
              <select  class="form-control" name="target">
                <option  value="_self" <?php if ($settings[0]->linktarget == "_self") {echo "selected";}?>   >Self</option>
                <option  value="_blank"  <?php if ($settings[0]->linktarget == "_blank") {echo "selected";}?>  >Blank</option>
              </select>
            </div>
          </div>
          <div class="row form-group">
            <label class="col-md-2 control-label text-left">Заголовок</label>
            <div class="col-md-4">
              <input type="text" name="headertitle" class="form-control" placeholder="title" value="<?php echo $settings[0]->header_title;?>" />
            </div>
          </div>
          <hr>
          <div class="row form-group">
            <label class="col-md-2 control-label text-left">Рекомендуемые</label>
            <div class="col-md-2">
              <input class="form-control" type="text" placeholder="" name="home"  value="6">
            </div>
            <label class="col-md-2 control-label text-left">Отображение порядка</label>
            <div class="col-md-3">
              <select class="form-control" name="homeorder">
                <option value="ol" label="По порядку" <?php if ($settings[0]->front_homepage_order == "ol") {echo "selected";}?>>По заказам</option>
                <option value="newf" label="По последним" <?php if ($settings[0]->front_homepage_order == "newf") {echo "selected";}?> >По последним первым</option>
                <option value="oldf" label="По старым" <?php if ($settings[0]->front_homepage_order == "oldf") {echo "selected";}?>>Старейшим первым</option>
                <option value="az" label="По возрастанию (A-Z)" <?php if ($settings[0]->front_homepage_order == "az") {echo "selected";}?>>По возрастанию (A-Z)</option>
                <option value="za" label="По убыванию (Z-A)" <?php if ($settings[0]->front_homepage_order == "za") {echo "selected";}?>>По убыванию (Z-A)</option>
              </select>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row form-group">
            <label class="col-md-2 control-label text-left">Список</label>
            <div class="col-md-2">
              <input class="form-control" type="text" placeholder="" name="listings"  value="<?php echo $settings[0]->front_listings;?>">
            </div>
            <label class="col-md-2 control-label text-left">Отобразить порядок</label>
            <div class="col-md-3">
              <select class="form-control" name="listingsorder">
                <option value="ol" label="По порядку" <?php if ($settings[0]->front_listings_order == "ol") {echo "selected";}?>>По заказу</option>
                <option value="newf" label="По последним" <?php if ($settings[0]->front_listings_order == "newf") {echo "selected";}?>>По последним первые</option>
                <option value="oldf" label="По старым" <?php if ($settings[0]->front_listings_order == "oldf") {echo "selected";}?>>Старейшие первые</option>
                <option value="az" label="По возрастанию (A-Z)" <?php if ($settings[0]->front_listings_order == "az") {echo "selected";}?>>По возрастанию (A-Z)</option>
                <option value="za" label="По убыванию (Z-A)" <?php if ($settings[0]->front_listings_order == "za") {echo "selected";}?>>В порядке убывания (Z-A)</option>
              </select>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row form-group">
            <label class="col-md-2 control-label text-left">Поиск</label>
            <div class="col-md-2">
              <input class="form-control" type="text" placeholder="" name="searchresult"  value="<?php echo $settings[0]->front_search;?>">
            </div>
            <label class="col-md-2 control-label text-left">Отобразить порядок</label>
            <div class="col-md-3">
              <select class="form-control" name="searchorder">
                <option value="ol" label="По порядку" <?php if ($settings[0]->front_search_order == "ol") {echo "selected";}?>>По заказу</option>
                <option value="newf" label="По последним" <?php if ($settings[0]->front_search_order == "newf") {echo "selected";}?>>По последним первые</option>
                <option value="oldf" label="По старым" <?php if ($settings[0]->front_search_order == "oldf") {echo "selected";}?>>Старейшие первые</option>
                <option value="az" label="По возрастанию (A-Z)" <?php if ($settings[0]->front_search_order == "az") {echo "selected";}?>>По возрастанию (A-Z)</option>
                <option value="za" label="По убыванию (Z-A)" <?php if ($settings[0]->front_search_order == "za") {echo "selected";}?>>В порядке убывания (Z-A)</option>
              </select>
            </div>
          </div>
          <div class="clearfix"></div>
          <!-- <div class="row form-group">
            <label class="col-md-2 control-label text-left">Popular Tours</label>
            <div class="col-md-2">
              <input class="form-control" type="text" placeholder="" name="popular"  value="<?php echo $settings[0]->front_popular;?>">
            </div>
            <label class="col-md-4 control-label text-left">Popuar tours are based on best reviews</label>
            <div class="col-md-3">
            </div>
          </div> -->
          <div class="row form-group">
            <label  class="col-md-2 control-label text-left">Связанные</label>
            <div class="col-md-2">
              <input class="form-control" type="text" placeholder="" name="related"  value="<?php echo $settings[0]->front_related;?>">
            </div>
          </div>
          <hr>
          <h4 class="text-danger">SEO</h4>
          <div class="row form-group">
            <label  class="col-md-2 control-label text-left">Ключевые слова</label>
            <div class="col-md-4">
              <input class="form-control" type="text" placeholder="" name="keywords" value="<?php echo $settings[0]->meta_keywords;?>">
            </div>
            <label  class="col-md-2 control-label text-left">Описание</label>
            <div class="col-md-4">
              <input class="form-control" type="text" placeholder="" name="description"  value="<?php echo $settings[0]->meta_description;?>">
            </div>
          </div>
          <hr>
          <h4 class="text-danger">Настройки поиска</h4>
          <div class="row form-group">
            <label  class="col-md-2 control-label text-left">Минимальная цена</label>
            <div class="col-md-2">
              <input class="form-control" type="text" placeholder="" name="minprice"  value="<?php echo $settings[0]->front_search_min_price;?>">
            </div>
            <label  class="col-md-2 control-label text-left">Максимальная цена</label>
            <div class="col-md-2">
              <input class="form-control" type="text" placeholder="" name="maxprice"  value="<?php echo $settings[0]->front_search_max_price;?>">
            </div>
          </div>
        </div>
        <div class="tab-pane wow fadeIn animated in" id="TOURTYPES">
          <div class="add_button_modal">
            <button type="button" data-toggle="modal" data-target="#ADD_TOURS_TYPES" class="btn btn-success"><i class="glyphicon glyphicon-plus-sign"></i> Добавить</button>
          </div>
          <div class="clearfix"></div>
       <?php echo $contentttypes; ?>
        </div>
        <div class="tab-pane wow fadeIn animated in" id="INCLUSIONS">
          <div class="add_button_modal">
            <button type="button" data-toggle="modal" data-target="#ADD_INCLUSIONS_TYPES" class="btn btn-success"><i class="glyphicon glyphicon-plus-sign"></i> Добавить</button>
          </div>
          <div class="clearfix"></div>
       <?php echo $contenttamenities; ?>
        </div>
        <div class="tab-pane wow fadeIn animated in" id="EXCLUSIONS">
          <div class="add_button_modal">
            <button type="button" data-toggle="modal" data-target="#ADD_EXCLUSIONS_TYPES" class="btn btn-success"><i class="glyphicon glyphicon-plus-sign"></i> Добавить</button>
          </div>
          <div class="clearfix"></div>
        <?php echo $contenttexclusions; ?>
        </div>
        <div class="tab-pane wow fadeIn animated in" id="PAYMENT">
          <div class="add_button_modal">
            <button type="button" data-toggle="modal" data-target="#ADD_PAYMENT_TYPES" class="btn btn-success"><i class="glyphicon glyphicon-plus-sign"></i> Добавить</button>
          </div>
          <div class="clearfix"></div>
         <?php echo $contenttpayments; ?>
        </div>
      </div>
    </div>
    <div class="panel-footer">
      <input name="updatesettings" value="1" type="hidden">
      <input name="updatefor" value="tours" type="hidden">
      <button type="submit" class="btn btn-primary">Сохранить</button>
    </div>
  </div>
</form>
<script>
  $(document).ready(function(){
  if(window.location.hash != "") {
  $('a[href="' + window.location.hash + '"]').click() } });
</script>
<!--Add payment types Modal -->
<div class="modal fade" id="ADD_PAYMENT_TYPES" tabindex="-1" role="dialog" aria-labelledby="ADD_PAYMENT_TYPES" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="" method="POST">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Добавить тип оплаты</h4>
        </div>
        <div class="modal-body form-horizontal">
          <div class="row form-group">
            <label  class="col-md-3 control-label text-left">Имя</label>
            <div class="col-md-8">
              <input type="text" name="name" class="form-control" placeholder="Name" value="" >
            </div>
          </div>
          <div class="row form-group">
            <label  class="col-md-3 control-label text-left">Выбрать</label>
            <div class="col-md-8">
              <select name="setselect" class="form-control" id="">
                <option value="Yes">Да</option>
                <option value="No">Нет</option>
              </select>
            </div>
          </div>
          <div class="row form-group">
            <label  class="col-md-3 control-label text-left">Статус</label>
            <div class="col-md-8">
              <select name="statusopt" class="form-control" id="">
                <option value="Yes">Вкл</option>
                <option value="No">Выкл</option>
              </select>
            </div>
          </div>
          <?php foreach($languages as $lang => $val){ if($lang != "en"){  ?>
          <div class="row form-group">
            <label  class="col-md-3 control-label text-left"> Имя <img src="<?php echo PT_LANGUAGE_IMAGES.$lang.".png"?>" height="20" alt="" />&nbsp;<?php echo $val['name'];?></label>
            <div class="col-md-8">
              <input type="text" name='<?php echo "translated[$lang][name]"; ?>' class="form-control" placeholder="Имя" value="" >
            </div>
          </div>
          <?php } } ?>
        </div>
        <div class="modal-footer">
          <input type="hidden" name="add" value="1" />
          <input type="hidden" name="typeopt" value="tpayments" />
          <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
          <button type="submit" class="btn btn-primary">Добавить</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!--end add payment types modal-->
<!--Add tour inclusions types Modal -->
<div class="modal fade" id="ADD_INCLUSIONS_TYPES" tabindex="-1" role="dialog" aria-labelledby="ADD_INCLUSIONS_TYPES" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="" method="POST" enctype="multipart/form-data" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Дополнительные услуги</h4>
        </div>
        <div class="modal-body form-horizontal">
          <div class="row form-group">
            <label  class="col-md-3 control-label text-left">Название</label>
            <div class="col-md-8">
              <input type="text" name="name" class="form-control" placeholder="Name" value="" >
            </div>
          </div>
          <div class="row form-group">
            <label  class="col-md-3 control-label text-left">Выбрать</label>
            <div class="col-md-8">
              <select name="setselect" class="form-control" id="">
                <option value="Yes">Да</option>
                <option value="No">Нет</option>
              </select>
            </div>
          </div>
          <div class="row form-group">
            <label  class="col-md-3 control-label text-left">Статус</label>
            <div class="col-md-8">
              <select name="statusopt" class="form-control" id="">
                <option value="Yes">Вкл</option>
                <option value="No">Выкл</option>
              </select>
            </div>
          </div>
          <?php foreach($languages as $lang => $val){ if($lang != "en"){   ?>
          <div class="row form-group">
            <label  class="col-md-3 control-label text-left"> Имя  <img src="<?php echo PT_LANGUAGE_IMAGES.$lang.".png"?>" height="20" alt="" />&nbsp;<?php echo $val['name'];?></label>
            <div class="col-md-8">
              <input type="text" name='<?php echo "translated[$lang][name]"; ?>' class="form-control" placeholder="Имя" value="" >
            </div>
          </div>
          <?php } } ?>
        </div>
        <div class="modal-footer">
          <input type="hidden" name="add" value="tamenities" />
          <input type="hidden" name="typeopt" value="tamenities" />
          <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
          <button type="submit" class="btn btn-primary">Добавить</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!---end add tour inclusion modal-->

<!--Add tour exclusions Modal -->
<div class="modal fade" id="ADD_EXCLUSIONS_TYPES" tabindex="-1" role="dialog" aria-labelledby="ADD_EXCLUSIONS_TYPES" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="" method="POST" enctype="multipart/form-data" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Исключения</h4>
        </div>
        <div class="modal-body form-horizontal">
          <div class="row form-group">
            <label  class="col-md-3 control-label text-left">Исключения</label>
            <div class="col-md-8">
              <input type="text" name="name" class="form-control" placeholder="Name" value="" >
            </div>
          </div>
          <div class="row form-group">
            <label  class="col-md-3 control-label text-left">Выбрать</label>
            <div class="col-md-8">
              <select name="setselect" class="form-control" id="">
                <option value="Yes">Да</option>
                <option value="No">Нет</option>
              </select>
            </div>
          </div>
          <div class="row form-group">
            <label  class="col-md-3 control-label text-left">Статус</label>
            <div class="col-md-8">
              <select name="statusopt" class="form-control" id="">
                <option value="Yes">Вкл</option>
                <option value="No">Выкл</option>
              </select>
            </div>
          </div>
          <?php foreach($languages as $lang => $val){ if($lang != "en"){   ?>
          <div class="row form-group">
            <label  class="col-md-3 control-label text-left"> Имя <img src="<?php echo PT_LANGUAGE_IMAGES.$lang.".png"?>" height="20" alt="" />&nbsp;<?php echo $val['name'];?></label>
            <div class="col-md-8">
              <input type="text" name='<?php echo "translated[$lang][name]"; ?>' class="form-control" placeholder="Name" value="" >
            </div>
          </div>
          <?php } } ?>
        </div>
        <div class="modal-footer">
          <input type="hidden" name="add" value="texclusions" />
          <input type="hidden" name="typeopt" value="texclusions" />
          <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
          <button type="submit" class="btn btn-primary">Добавить</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!---end add tour exclusion modal-->

<!--Add tour type types Modal -->
<div class="modal fade" id="ADD_TOURS_TYPES" tabindex="-1" role="dialog" aria-labelledby="ADD_TOURS_TYPES" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="" method="POST" enctype="multipart/form-data" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Добавить тип</h4>
        </div>
        <div class="modal-body form-horizontal">
          <div class="row form-group">
            <label  class="col-md-3 control-label text-left">Имя</label>
            <div class="col-md-8">
              <input type="text" name="name" class="form-control" placeholder="Name" value="" >
            </div>
          </div>

          <div class="row form-group">
            <label  class="col-md-3 control-label text-left">Статус</label>
            <div class="col-md-8">
              <select name="statusopt" class="form-control" id="">
                <option value="Yes">Вкл</option>
                <option value="No">Выкл</option>
              </select>
            </div>
          </div>
          <?php foreach($languages as $lang => $val){ if($lang != "en"){   ?>
          <div class="row form-group">
            <label  class="col-md-3 control-label text-left"> Имя <img src="<?php echo PT_LANGUAGE_IMAGES.$lang.".png"?>" height="20" alt="" />&nbsp;<?php echo $val['name'];?></label>
            <div class="col-md-8">
              <input type="text" name='<?php echo "translated[$lang][name]"; ?>' class="form-control" placeholder="Имя" value="" >
            </div>
          </div>
          <?php } } ?>
        </div>
        <div class="modal-footer">
          <input type="hidden" name="add" value="ttypes" />
          <input type="hidden" name="typeopt" value="ttypes" />
          <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
          <button type="submit" class="btn btn-primary">Добавить</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!---end add tour type modal-->

<!-- Edit Modal -->
<?php foreach($typeSettings as $ts){ ?>
<div class="modal fade" id="sett<?php echo $ts->sett_id;?>" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form action="" method="POST" enctype="multipart/form-data">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Обновить <?php echo $ts->sett_name;?></h4>
        </div>
        <div class="modal-body form-horizontal">
          <div class="row form-group">
            <label  class="col-md-3 control-label text-left">Имя</label>
            <div class="col-md-8">
              <input type="text" name="name" class="form-control" placeholder="Name" value="<?php echo $ts->sett_name;?>" >
            </div>
          </div>

          <div class="row form-group">
            <label  class="col-md-3 control-label text-left">Выбрать</label>
            <div class="col-md-8">
              <select name="setselect" class="form-control" id="">
                <option value="Yes" <?php makeSelected($ts->sett_selected,"Yes"); ?> >Да</option>
                <option value="No" <?php makeSelected($ts->sett_selected,"No"); ?>  >Нет</option>
              </select>
            </div>
          </div>

          <div class="row form-group">
            <label  class="col-md-3 control-label text-left">Статус</label>
            <div class="col-md-8">
              <select name="statusopt" class="form-control" id="">
                <option value="Yes" <?php makeSelected($ts->sett_status,"Yes"); ?> >Вкл</option>
                <option value="No" <?php makeSelected($ts->sett_status,"No"); ?>  >Выкл</option>
              </select>
            </div>
          </div>
          <?php foreach($languages as $lang => $val){ if($lang != "en"){ @$trans = getTypesTranslation($lang, $ts->sett_id); ?>
          <div class="row form-group">
            <label  class="col-md-3 control-label text-left"> Имя <img src="<?php echo PT_LANGUAGE_IMAGES.$lang.".png"?>" height="20" alt="" />&nbsp;<?php echo $val['name'];?></label>
            <div class="col-md-8">
              <input type="text" name='<?php echo "translated[$lang][name]"; ?>' class="form-control" placeholder="Имя" value="<?php echo @$trans[0]->trans_name;?>" >
            </div>
          </div>
          <?php } } ?>
        </div>
        <div class="modal-footer">
          <input type="hidden" name="updatetype" value="1" />
          <input type="hidden" name="settid" value="<?php echo $ts->sett_id;?>" />
          <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
          <button type="submit" class="btn btn-primary">Обновление</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php } ?>

<!----edit modal--->
