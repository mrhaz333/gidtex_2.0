<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$L1['form_validation_required'] = 'Поле {field} обязательно для заполнения.';
$L1['form_validation_isset'] = 'Поле {field} должно иметь значение.';
$L1['form_validation_valid_email'] = 'Поле {field} должно содержать действительный адрес электронной почты.';
$L1['form_validation_valid_emails'] = 'Поле {field} должно содержать все действительные адреса электронной почты.';
$L1['form_validation_valid_url'] = 'Поле {field} должно содержать действительный URL.';
$L1['form_validation_valid_ip'] = 'Поле {field} должно содержать действительный IP.';
$L1['form_validation_min_length'] = 'Длина поля {field} должна быть не менее {param} символов.';
$L1['form_validation_max_length'] = 'Длина поля {field} не может превышать {param} символов.';
$L1['form_validation_exact_length'] = 'Поле {field} должно быть точно длиной {param} символов.';
$L1['form_validation_alpha'] = 'Поле {field} может содержать только буквы алфавита.';
$L1['form_validation_alpha_numeric'] = 'Поле {field} может содержать только буквенно-цифровые символы.';
$L1['form_validation_alpha_numeric_spaces'] = 'Поле {field} может содержать только буквенно-цифровые символы и пробелы.';
$L1['form_validation_alpha_dash'] = 'Поле {field} может содержать только буквенно-цифровые символы, подчеркивания и тире.';
$L1['form_validation_numeric'] = 'Поле {field} должно содержать только цифры.';
$L1['form_validation_is_numeric'] = 'Поле {field} должно содержать только числовые символы.';
$L1['form_validation_integer'] = 'Поле {field} должно содержать целое число.';
$L1['form_validation_regex_match'] = 'Поле {field} имеет неправильный формат.';
$L1['form_validation_matches'] = 'Поле {field} не соответствует полю {param}.';
$L1['form_validation_differs'] = 'Поле {field} должно отличаться от поля {param}.';
$L1['form_validation_is_unique'] = 'Поле {field} должно содержать уникальное значение.';
$L1['form_validation_is_natural'] = 'Поле {field} должно содержать только цифры.';
$L1['form_validation_is_natural_no_zero'] = 'Поле {field} должно содержать только цифры и должно быть больше нуля.';
$L1['form_validation_decimal'] = 'Поле {field} должно содержать десятичное число.';
$L1['form_validation_less_than'] = 'Поле {field} должно содержать число меньше, чем {param}.';
$L1['form_validation_less_than_equal_to'] = 'Поле {field} должно содержать число, меньшее или равное {param}.';
$L1['form_validation_greater_than'] = 'Поле {field} должно содержать число больше, чем {param}.';
$L1['form_validation_greater_than_equal_to'] = 'Поле {field} должно содержать число, большее или равное {param}.';
$L1['form_validation_error_message_not_set'] = 'Невозможно получить доступ к сообщению об ошибке, соответствующему имени вашего поля {field}.';
$L1['form_validation_in_list'] = 'Поле {field} должно быть одним из: {param}.';
